import allure
import pytest
import yaml

def get_datas():
    with open("./datas/calc.yml")as f:
        datas = yaml.safe_load(f)
    return  datas

@allure.feature("测试计算器")
class TestCal:
    """
    提取参数放在装饰器中动态形成执行测试用例
    """
    @pytest.mark.parametrize('a,b,expect',get_datas()['add_int']['datas'])
    @allure.story("测试相加功能_int")
    def test_add(self,calculate,a,b,expect):
        assert expect == calculate.add(a,b)


    @pytest.mark.parametrize('a,b,expect',get_datas()['add_float']['datas'],ids=get_datas()['add_float']['ids'])
    @allure.story("测试相加功能_float")
    def test_add_float(self,calculate,a,b,expect):
        assert expect == round(calculate.add(a,b),2)

    @pytest.mark.parametrize('a,b,expect',get_datas()['add_negative']['datas'])
    @allure.story("测试相加功能_negative")
    def test_add_negative(self,calculate,a,b,expect):
        assert expect == calculate.add(a,b)

    @pytest.mark.parametrize('a,b,expect',get_datas()['add_mixture']['datas'])
    @allure.story("测试相加功能_mixture")
    def test_add_mixture(self,calculate,a,b,expect):
        assert expect == round(calculate.add(a,b),2)

    @pytest.mark.parametrize('a,b,expect', get_datas()['add_characters']['datas'])
    @allure.story("测试相加功能_特殊字符")
    def test_add_characters(self, calculate, a, b, expect):
         try:
             calculate.add(a,b,expect)
         except TypeError:
             print("只支持int和float类型")



    @pytest.mark.parametrize('a,b', get_datas()['div_divisor_zero']['datas'])
    @allure.story("测试相除功能_0为除数")
    def test_div(self,calculate, a, b):
        # try:
        #     cal.div(1,0)
        # except ZeroDivisionError:
        #     print("除数为0")
        with pytest.raises(ZeroDivisionError):
            calculate.div(a, b)

    @pytest.mark.parametrize('a,b,expect', get_datas()['div_dividend_zero']['datas'])
    @allure.story("测试相除功能_0为被除数")
    def test_div_dividend_zero(self,calculate, a, b,expect):
        assert expect == calculate.div(a, b)

    @pytest.mark.parametrize('a,b,expect', get_datas()['div_float']['datas'])
    @allure.story("测试相除功能_浮点数")
    def test_div_float(self,calculate, a, b,expect):
        assert expect == round(calculate.div(a,b),2)


    @pytest.mark.parametrize('a,b,expect', get_datas()['div_positive']['datas'])
    @allure.story("测试相除功能_正数")
    def test_div_positive(self,calculate, a, b,expect):
        assert expect == round(calculate.div(a,b),2)

    @pytest.mark.parametrize('a,b,expect', get_datas()['div_negative']['datas'])
    @allure.story("测试相除功能_负数")
    def test_div_negative(self,calculate, a, b,expect):
        assert expect == round(calculate.div(a,b),2)

    @pytest.mark.parametrize('a,b', get_datas()['div_characters']['datas'])
    @allure.story("测试相除功能_特殊字符")
    def test_div_characters(self, calculate, a, b):
        try:
            calculate.add(a, b)
        except TypeError:
            print("只支持int和float类型")

